import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './LoadingIndicator.styl';

class LoadingIndicator extends PureComponent {
  static propTypes = {
    percentComplete: PropTypes.number.isRequired,
    error: PropTypes.object
  };

  static defaultProps = {
    percentComplete: 0,
    error: null
  };

  render() {
    return (
      <div>
        {this.props.error ? (
          <div className="imageViewerErrorLoadingIndicator loadingIndicator">
            <div className="indicatorContents">
              <h4>No image to display</h4>
              <p className="details">{this.props.error.message}</p>
            </div>
          </div>
        ) : (
          <div className="imageViewerLoadingIndicator loadingIndicator">
            <div className="indicatorContents">
              <p>Loading...</p>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default LoadingIndicator;
